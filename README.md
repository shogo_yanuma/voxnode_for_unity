** MagicaVoxelのvoxファイルをUnityで表示するためのサンプルコード **

MagicaVoxel

[https://ephtracy.github.io/](https://ephtracy.github.io/)



---

## Quick Start

Assets/Scenes/SampleScene.unity を開いて実行してみてください。


---

## 使い方

1. 表示したいvoxファイルを Assets/StreamingAssets 以下に配置
2. Assets/voxnode/prefab/VOXNode をシーンに配置
3. シーンに置かれた VOXNode のfilenameプロパティにvoxファイルへのパスを指定する（指定がない場合：/vox/test.vox）


---

## トラブルシューティング

Q. モデルの影に隙間ができる

A. Light設定のRealtimeshadows / Normal Biasに0を指定すると影のギャップが消えます。


---

## 課題

1. 複数ブロックを含むvoxファイルは最初のブロックしか表示されない。


