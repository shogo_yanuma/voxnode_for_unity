﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;   // for List<>
using System.IO;	// FileSystem
using System;	// Exception
using System.Text;	// Encoding

public class VOXFileLoader
{
	private BinaryReader _reader;
	private VOXFileVO _fileVO;

	private string _loadText;


	public VOXFileVO loadFromVOXBytes(byte[] bytes)
	{
		MemoryStream ms = new MemoryStream(bytes);
		_reader = new BinaryReader(ms);
		_parseVOXFile();
		return _fileVO;
	}

	private void _parseVOXFile()
	{
		_fileVO = new VOXFileVO();

		_checkVox();

		var baseStream = _reader.BaseStream;

		char[] id;
		int chunk_size;
		int child_size;

		VOXBlockVO blockVO = new VOXBlockVO();

		while (baseStream.Position != baseStream.Length) {

			long pos = baseStream.Position;
			long len = baseStream.Length;

			// ヘッダ部の読み込み
			id = _reader.ReadChars(4);
			string chunk_id = id[0].ToString() + id[1].ToString() + id[2].ToString() + id[3].ToString();
			if (id[0] == 0) {
				// eof
				break;
			}

			chunk_size = _reader.ReadInt32();
			child_size = _reader.ReadInt32();
//			string chunk_id = String.Format("%c%c%c%c", id[0], id[1], id[2], id[3]);

			if (chunk_id == "SIZE") {
				// サイズ
				int x, y, z;
				x = _reader.ReadInt32();
				y = _reader.ReadInt32();
				z = _reader.ReadInt32();

				_fileVO.header.width = x;
				_fileVO.header.depth = y;
				_fileVO.header.height = z;

				int cnt = x * y * z;
				_fileVO.generateEmptyBlocks(cnt);
			}
			else if (chunk_id == "XYZI") {
			  // 配置されたボクセル
      	// x, y, z, color
				int num = _reader.ReadInt32();
				for (int i=0; i<num; i++) {
					byte[] data = _reader.ReadBytes(4);
					if (data[3] == 0) continue;

					blockVO.colorIndex = (short)(data[3] - 1);
					_fileVO.setBlock(data[0], data[1], data[2], blockVO);
				}
			}
			else if (chunk_id == "RGBA") {
				// 色。256色固定(r, g, b, a)
				for (int i=0; i<255; i++) {
					byte[] data = _reader.ReadBytes(4);
					VOXColorVO colVO = new VOXColorVO();
					colVO.index = i;
					colVO.color = new Color();
					colVO.color.r = ((float)data[0])/255.0f;
					colVO.color.g = ((float)data[1])/255.0f;
					colVO.color.b = ((float)data[2])/255.0f;
					colVO.color.a = ((float)data[3])/255.0f;
					_fileVO.colors.Add(colVO);
				}
			}
			else {
				// 上記以外のチャンクはスキップ
				_reader.ReadBytes(chunk_size);
			}
		}
	}

	private bool _checkVox()
	{
		char[] id = _reader.ReadChars(4);
		int version = _reader.ReadInt32();
		string chunk_id = id[0].ToString() + id[1].ToString() + id[2].ToString() + id[3].ToString();
		  //String.Format("%c%c%c%c", id[0], id[1], id[2], id[3]);
		return (chunk_id == "VOX ");
	}

}
