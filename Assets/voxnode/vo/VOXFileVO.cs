using System.Collections.Generic;

public class VOXFileVO
{
	public string filename;
	public string modelPath;

	public VOXHeaderVO header;
	public List<VOXColorVO> colors;
	public List<VOXBlockVO> pixels;

	private static VOXBlockVO _emptyVO = new VOXBlockVO();

	public VOXFileVO() {
		header = new VOXHeaderVO();
		colors = new List<VOXColorVO>();
		pixels = new List<VOXBlockVO>();
	}

	public VOXColorVO getColor(int idx) {
		int cnt = colors.Count;
		for (int i=0; i<cnt; i++) {
			VOXColorVO col = colors[i];
			if (col.index == idx) {
				return col;
			}
		}
		VOXColorVO dmy = new VOXColorVO();
		return dmy;
	}

	public VOXBlockVO getBlock(int w, int d, int h) {
		if (w<0 || w>=header.width || d<0 || d>=header.depth || h<0 || h>=header.height) {
			return _emptyVO;
		}
		int idx = h * (header.width*header.depth) + d*header.width + w;
		VOXBlockVO vo = pixels[idx];
		return vo;
	}

	public void setBlock(int w, int d, int h, VOXBlockVO vo) {
		int idx = h * (header.width*header.depth) + d*header.width + w;
		pixels[idx].initWith( vo );
	}

	public void generateEmptyBlocks(int cnt) {
		pixels.Clear();

		for (int i=0; i<cnt; i++) {
			VOXBlockVO blockVO = new VOXBlockVO();
			pixels.Add( blockVO );
		}
	}

}
