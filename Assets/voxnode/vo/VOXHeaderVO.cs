using System.Collections.Generic;

public class VOXHeaderVO
{
  public string author;
  public string title;

  public int width;   // x
  public int depth;   // z
  public int height;  // y

  public VOXHeaderVO() {
  }
}
