using UnityEngine;
using System.IO;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshCollider))]

class VOXNode : MonoBehaviour
{
  private VOXFileVO _voxFileVO;

  private MeshFilter meshFilter;
  private Mesh myMesh;
  private MeshCollider meshCollider;

  public float blockSize = 0.2f;  // 1.0 = 1m
  public PhysicMaterial physicMaterial;

  public string filename;

  void Start()
  {
    meshFilter = gameObject.GetComponent<MeshFilter>();
    meshCollider = gameObject.GetComponent<MeshCollider>();
    myMesh = new Mesh();

    string path;
    if (filename.Length == 0) {
      path = Application.streamingAssetsPath + "/vox/test.vox";
    }
    else {
      path = Application.streamingAssetsPath + filename;
    }

    string ext = Path.GetExtension(path);

      // .vox file
      VOXFileLoader loader = new VOXFileLoader();

#if UNITY_ANDROID
      // Androidの場合はSystem.IOで直接ファイルとしてロードすることができないため、UnityWebRequestを用いる
      var request = UnityWebRequest.Get(path);
      // 注：標準ではawaitできないので何らか対応する必要がある
      await request.SendWebRequest();
      byte[] pixData = request.downloadHandler.data;
#else
      byte[] pixData = File.ReadAllBytes(path);
#endif

      VOXFileVO vo = loader.loadFromVOXBytes(pixData);
      initWith(vo);

    meshCollider.sharedMesh = myMesh;
  }

  public bool initWith(VOXFileVO vo)
  {
    setVOXFileVO(vo);
    return true;
  }

  public void setVOXFileVO(VOXFileVO vo)
  {
    _voxFileVO = vo;

    Debug.Log("file:" + _voxFileVO.filename + " / cols:" + _voxFileVO.colors.Count + ", pix:" + _voxFileVO.pixels.Count);

    _buildVOXMesh_vertPerBlock();
  }


  private void _buildVOXMesh_vertPerBlock()
  {
    Debug.Log("_buildVOXMesh_vertPerBlock");
    int vertCnt = 0;
    int triCnt = 0;

    for (int h=0; h<_voxFileVO.header.height; h++) {
      for (int d=0; d<_voxFileVO.header.depth; d++) {
        for (int w=0; w<_voxFileVO.header.width; w++) {
          VOXBlockVO currVO = _voxFileVO.getBlock(w, d, h);
          if (currVO.colorIndex != 0) {
            // 上面
            VOXBlockVO upVO = _voxFileVO.getBlock(w, d, h+1);
            if (upVO.colorIndex == 0) {
              vertCnt += 4;
              triCnt += 6;
            }
            // 下面
            VOXBlockVO btmVO = _voxFileVO.getBlock(w, d, h-1);
            if (btmVO.colorIndex == 0) {
              vertCnt += 4;
              triCnt += 6;
            }
            // 左面
            VOXBlockVO leftVO = _voxFileVO.getBlock(w-1, d, h);
            if (leftVO.colorIndex == 0) {
              vertCnt += 4;
              triCnt += 6;
            }
            // 右面
            VOXBlockVO rightVO = _voxFileVO.getBlock(w+1, d, h);
            if (rightVO.colorIndex == 0) {
              vertCnt += 4;
              triCnt += 6;
            }
            // 正面
            VOXBlockVO frontVO = _voxFileVO.getBlock(w, d-1, h);
            if (frontVO.colorIndex == 0) {
              vertCnt +=4;
              triCnt += 6;
            }
            // 背面
            VOXBlockVO backVO = _voxFileVO.getBlock(w, d+1, h);
            if (backVO.colorIndex == 0) {
              vertCnt +=4;
              triCnt += 6;
            }
          }
        }
      }
    }

    Vector3[] verts = new Vector3[vertCnt];
    int[] triangles = new int[triCnt];
    Color[] cols = new Color[vertCnt];
    Vector3[] normals = new Vector3[vertCnt];

    int vertIdx = 0;
    int triangleIdx = 0;

    Vector3 startPos = new Vector3(-blockSize*_voxFileVO.header.width/2, blockSize/2, -blockSize*_voxFileVO.header.depth/2);
    Vector3 pos;
    float halfBlockSize = blockSize/2;

    for (int h=0; h<_voxFileVO.header.height; h++) {
      for (int d=0; d<_voxFileVO.header.depth; d++) {
        for (int w=0; w<_voxFileVO.header.width; w++) {
          VOXBlockVO currVO = _voxFileVO.getBlock(w, d, h);
          if (currVO.colorIndex != 0) {

            VOXColorVO col = _voxFileVO.getColor(currVO.colorIndex);

            pos.x = startPos.x + w * blockSize;
            pos.y = startPos.y + h * blockSize;
            pos.z = startPos.z + d * blockSize;

            // 上面
            VOXBlockVO upVO = _voxFileVO.getBlock(w, d, h+1);
            if (upVO.colorIndex == 0) {
              verts[vertIdx] = new Vector3(pos.x-halfBlockSize, pos.y+halfBlockSize, pos.z+halfBlockSize);
              verts[vertIdx+1] = new Vector3(pos.x+halfBlockSize, pos.y+halfBlockSize, pos.z+halfBlockSize);
              verts[vertIdx+2] = new Vector3(pos.x+halfBlockSize, pos.y+halfBlockSize, pos.z-halfBlockSize);
              verts[vertIdx+3] = new Vector3(pos.x-halfBlockSize, pos.y+halfBlockSize, pos.z-halfBlockSize);

              normals[vertIdx] = normals[vertIdx+1] = normals[vertIdx+2] = normals[vertIdx+3] = Vector3.up;

              triangles[triangleIdx] = vertIdx;
              triangles[triangleIdx+1] = vertIdx+1;
              triangles[triangleIdx+2] = vertIdx+2;
              triangles[triangleIdx+3] = vertIdx;
              triangles[triangleIdx+4] = vertIdx+2;
              triangles[triangleIdx+5] = vertIdx+3;

              cols[vertIdx] = col.color;
              cols[vertIdx+1] = col.color;
              cols[vertIdx+2] = col.color;
              cols[vertIdx+3] = col.color;

              vertIdx += 4;
              triangleIdx += 6;
            }

            // 下面
            VOXBlockVO btmVO = _voxFileVO.getBlock(w, d, h-1);
            if (btmVO.colorIndex == 0) {
              verts[vertIdx] = new Vector3(pos.x+halfBlockSize, pos.y-halfBlockSize, pos.z+halfBlockSize);
              verts[vertIdx+1] = new Vector3(pos.x-halfBlockSize, pos.y-halfBlockSize, pos.z+halfBlockSize);
              verts[vertIdx+2] = new Vector3(pos.x-halfBlockSize, pos.y-halfBlockSize, pos.z-halfBlockSize);
              verts[vertIdx+3] = new Vector3(pos.x+halfBlockSize, pos.y-halfBlockSize, pos.z-halfBlockSize);

              normals[vertIdx] = normals[vertIdx+1] = normals[vertIdx+2] = normals[vertIdx+3] = Vector3.down;

              triangles[triangleIdx] = vertIdx;
              triangles[triangleIdx+1] = vertIdx+1;
              triangles[triangleIdx+2] = vertIdx+2;
              triangles[triangleIdx+3] = vertIdx;
              triangles[triangleIdx+4] = vertIdx+2;
              triangles[triangleIdx+5] = vertIdx+3;

              cols[vertIdx] = col.color;
              cols[vertIdx+1] = col.color;
              cols[vertIdx+2] = col.color;
              cols[vertIdx+3] = col.color;

              vertIdx += 4;
              triangleIdx += 6;
            }

            // 左面
            VOXBlockVO leftVO = _voxFileVO.getBlock(w-1, d, h);
            if (leftVO.colorIndex == 0) {
              verts[vertIdx] = new Vector3(pos.x-halfBlockSize, pos.y+halfBlockSize, pos.z+halfBlockSize);
              verts[vertIdx+1] = new Vector3(pos.x-halfBlockSize, pos.y+halfBlockSize, pos.z-halfBlockSize);
              verts[vertIdx+2] = new Vector3(pos.x-halfBlockSize, pos.y-halfBlockSize, pos.z-halfBlockSize);
              verts[vertIdx+3] = new Vector3(pos.x-halfBlockSize, pos.y-halfBlockSize, pos.z+halfBlockSize);

              normals[vertIdx] = normals[vertIdx+1] = normals[vertIdx+2] = normals[vertIdx+3] = Vector3.left;

              triangles[triangleIdx] = vertIdx;
              triangles[triangleIdx+1] = vertIdx+1;
              triangles[triangleIdx+2] = vertIdx+2;
              triangles[triangleIdx+3] = vertIdx;
              triangles[triangleIdx+4] = vertIdx+2;
              triangles[triangleIdx+5] = vertIdx+3;

              cols[vertIdx] = col.color;
              cols[vertIdx+1] = col.color;
              cols[vertIdx+2] = col.color;
              cols[vertIdx+3] = col.color;

              vertIdx += 4;
              triangleIdx += 6;
            }

            // 右面
            VOXBlockVO rightVO = _voxFileVO.getBlock(w+1, d, h);
            if (rightVO.colorIndex == 0) {
              verts[vertIdx] = new Vector3(pos.x+halfBlockSize, pos.y+halfBlockSize, pos.z-halfBlockSize);
              verts[vertIdx+1] = new Vector3(pos.x+halfBlockSize, pos.y+halfBlockSize, pos.z+halfBlockSize);
              verts[vertIdx+2] = new Vector3(pos.x+halfBlockSize, pos.y-halfBlockSize, pos.z+halfBlockSize);
              verts[vertIdx+3] = new Vector3(pos.x+halfBlockSize, pos.y-halfBlockSize, pos.z-halfBlockSize);

              normals[vertIdx] = normals[vertIdx+1] = normals[vertIdx+2] = normals[vertIdx+3] = Vector3.right;

              triangles[triangleIdx] = vertIdx;
              triangles[triangleIdx+1] = vertIdx+1;
              triangles[triangleIdx+2] = vertIdx+2;
              triangles[triangleIdx+3] = vertIdx;
              triangles[triangleIdx+4] = vertIdx+2;
              triangles[triangleIdx+5] = vertIdx+3;

              cols[vertIdx] = col.color;
              cols[vertIdx+1] = col.color;
              cols[vertIdx+2] = col.color;
              cols[vertIdx+3] = col.color;

              vertIdx += 4;
              triangleIdx += 6;
            }

            // 正面
            VOXBlockVO frontVO = _voxFileVO.getBlock(w, d-1, h);
            if (frontVO.colorIndex == 0) {
              verts[vertIdx] = new Vector3(pos.x-halfBlockSize, pos.y+halfBlockSize, pos.z-halfBlockSize);
              verts[vertIdx+1] = new Vector3(pos.x+halfBlockSize, pos.y+halfBlockSize, pos.z-halfBlockSize);
              verts[vertIdx+2] = new Vector3(pos.x+halfBlockSize, pos.y-halfBlockSize, pos.z-halfBlockSize);
              verts[vertIdx+3] = new Vector3(pos.x-halfBlockSize, pos.y-halfBlockSize, pos.z-halfBlockSize);

              normals[vertIdx] = normals[vertIdx+1] = normals[vertIdx+2] = normals[vertIdx+3] = Vector3.back;

              triangles[triangleIdx] = vertIdx;
              triangles[triangleIdx+1] = vertIdx+1;
              triangles[triangleIdx+2] = vertIdx+2;
              triangles[triangleIdx+3] = vertIdx;
              triangles[triangleIdx+4] = vertIdx+2;
              triangles[triangleIdx+5] = vertIdx+3;

              cols[vertIdx] = col.color;
              cols[vertIdx+1] = col.color;
              cols[vertIdx+2] = col.color;
              cols[vertIdx+3] = col.color;

              vertIdx += 4;
              triangleIdx += 6;
            }

            // 背面
            VOXBlockVO backVO = _voxFileVO.getBlock(w, d+1, h);
            if (backVO.colorIndex == 0) {
              verts[vertIdx] = new Vector3(pos.x+halfBlockSize, pos.y+halfBlockSize, pos.z+halfBlockSize);
              verts[vertIdx+1] = new Vector3(pos.x-halfBlockSize, pos.y+halfBlockSize, pos.z+halfBlockSize);
              verts[vertIdx+2] = new Vector3(pos.x-halfBlockSize, pos.y-halfBlockSize, pos.z+halfBlockSize);
              verts[vertIdx+3] = new Vector3(pos.x+halfBlockSize, pos.y-halfBlockSize, pos.z+halfBlockSize);

              normals[vertIdx] = normals[vertIdx+1] = normals[vertIdx+2] = normals[vertIdx+3] = Vector3.forward;

              triangles[triangleIdx] = vertIdx;
              triangles[triangleIdx+1] = vertIdx+1;
              triangles[triangleIdx+2] = vertIdx+2;
              triangles[triangleIdx+3] = vertIdx;
              triangles[triangleIdx+4] = vertIdx+2;
              triangles[triangleIdx+5] = vertIdx+3;

              cols[vertIdx] = col.color;
              cols[vertIdx+1] = col.color;
              cols[vertIdx+2] = col.color;
              cols[vertIdx+3] = col.color;

              vertIdx += 4;
              triangleIdx += 6;
            }
          }
        }
      }
    }

    myMesh.vertices = verts;
    myMesh.colors = cols;
    myMesh.triangles = triangles;
    myMesh.normals = normals;

    meshFilter.mesh = myMesh;
  }

}
